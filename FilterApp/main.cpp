//
//  main.cpp
//  FilterApp
//
//  Created by Hovhannes Grigoryan on 11/28/19.
//  Copyright © 2019 Hovhannes Grigoryan. All rights reserved.
//

#include <iostream>
#include <sstream>

#include "Core/Image/Image.h"

class FilterManager {
public:
    FilterManager() {}
    
    ImagePtr applyFilter(ImagePtr img) {
        return ImagePtr(new Image("test"));
    }
};

Image f(Image img) { return img; }

int main(int argc, const char* argv[]) {
    Image img("img");
}
