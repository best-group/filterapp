//
//  Image.h
//  FilterApp
//
//  Created by Hovhannes Grigoryan on 11/28/19.
//  Copyright © 2019 Hovhannes Grigoryan. All rights reserved.
//

#ifndef __IMAGE_H__
#define __IMAGE_H__

#include <iostream>
#include <sstream>

class Image {
public:
    // Default constructor
    Image() = delete;
    
    Image(const std::string& n);
    
    // Copy constructor
    Image(const Image& img);
    
    // Move constructor
    Image(Image&& img);
    
    ~Image();
    
    // This non member function is our firend
    friend std::ostream& operator<<(std::ostream& os, const Image& img);

private:
    char* content;
    std::size_t size;
    int id;
    std::string name;
    
    // Global id register
    static int globalId;
};

typedef std::shared_ptr<Image> ImagePtr;

std::ostream& operator<<(std::ostream& os, const Image& img);

#endif//__IMAGE_H__
