//
//  Image.cpp
//  FilterApp
//
//  Created by Hovhannes Grigoryan on 11/28/19.
//  Copyright © 2019 Hovhannes Grigoryan. All rights reserved.
//

#include "Image.h"

int Image::globalId = 0;

Image::Image(const std::string& n)
    : content(nullptr)
    , size(0)
    , id(++globalId)
    , name(n) {
    
    std::ostringstream oss;
    oss << "image_" << id;
    const std::string str = oss.str();
    size  = str.size();
    content = new char[size];
    for(int i = 0; i < size; ++i) {
        content[i] = str[i];
    }
        
    std::cout << "Create " << *this << std::endl;
}

Image::Image(const Image& img) {
    // Copy the size first
    size = img.size;
    name = img.name;
    
    // Now copy the content
    content = new char[size];
    for(int i = 0; i < size; ++i) {
        content[i] = img.content[i];
    }
    
    id = ++globalId;
    std::cout << "Create " << *this << " from: " << img << std::endl;
}

Image::Image(Image&& img) {
    std::cout << "Move from: " << img << std::endl;
    
    id = img.id;
    size = img.size;
    content = img.content;
    
    name = std::move(img.name);
    
    img.content = nullptr;
    img.size = 0;
    img.id = 0;
}

Image::~Image() {
    std::cout << "Destroy " << *this << std::endl;
    
    delete [] content;
    size = 0;
}

std::ostream& operator<<(std::ostream& os, const Image& img) {
    os << "Id: " << img.id << ", name: " << img.name << ", content: ";
    for(int i = 0; i < img.size; ++i) {
        os << img.content[i];
    }
    return os;
}
